package uk.lambocreeper.elementary.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Me implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("§cPlease define a message.");
        } else {
            String msg = "";
            for (int i = 0; i != args.length; i++) {
                msg += args[i] + " ";
            }
            String _rC = "§e";
            if (sender.isOp()) {
                _rC = "§c";
            }
            Bukkit.broadcastMessage(_rC + sender.getName() + "§e§o " + msg);
        }
        return true;
    }
}
