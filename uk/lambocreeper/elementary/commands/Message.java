package uk.lambocreeper.elementary.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class Message implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage("§cPlease define a recipient and message.");
        } else {
            String msg = "";
            String _rC = "§e";
            String _pC = "§e";
            for (int i = 1; i != args.length; i++) {
                msg += args[i] + " ";
            }
            Player _recipient;
            for (Player _p: Bukkit.getOnlinePlayers()) {
                if (_p.getName().toUpperCase().equals(args[0].toUpperCase())) {
                    _recipient = Bukkit.getPlayer(args[0]);
                    if (_recipient.isOp()) {
                        _rC = "§c";
                    }
                    if (sender.isOp()) {
                        _pC = "§c";
                    }
                    _recipient.sendMessage("§eFrom " + _pC + sender.getName() + "§e: §f" + msg);
                    sender.sendMessage("§eTo " + _rC + _recipient.getName() + "§e: §f" + msg);
                } else {
                    ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
                    if (args[0].toUpperCase().equals("CONSOLE")) {
                        console.sendMessage("§eFrom " + _pC + sender.getName() + "§e: §f" + msg);
                        sender.sendMessage("§eTo §cCONSOLE§e: §f" + msg);
                    } else {
                        sender.sendMessage("§cThat player is offline.");
                    }
                }
            }
        }
        return true;
    }
}
