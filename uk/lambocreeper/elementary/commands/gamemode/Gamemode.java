package uk.lambocreeper.elementary.commands.gamemode;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Gamemode implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                TextComponent _title = new TextComponent("§6Elementary §7(Gamemode)\n");
                _title.setColor(ChatColor.GRAY);
                _title.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://lambocreeper.uk/elementary/docs?ref=mc&cmd=gamemode"));
                _title.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7§oClick to view the documentation for this command.").create()));

                TextComponent _about = new TextComponent("§eChange your gamemode.");
                _about.setColor(ChatColor.YELLOW);

                player.spigot().sendMessage(new TextComponent[]{_title, _about});
            } else {
                switch(args[0].toUpperCase()) {
                    case "0":
                    case "S":
                    case "SURVIVAL": {
                        player.setGameMode(GameMode.SURVIVAL);
                        player.sendMessage("§eYour gamemode has been updated to §lSurvival§e.");
                        break;
                    }
                    case "1":
                    case "C":
                    case "CREATIVE": {
                        player.setGameMode(GameMode.CREATIVE);
                        player.sendMessage("§eYour gamemode has been updated to §lCreative§e.");
                        break;
                    }
                    case "2":
                    case "A":
                    case "ADVENTURE": {
                        player.setGameMode(GameMode.ADVENTURE);
                        player.sendMessage("§eYour gamemode has been updated to §lAdventure§e.");
                        break;
                    }
                    case "3":
                    case "SP":
                    case "SPECTATOR": {
                        player.setGameMode(GameMode.SPECTATOR);
                        player.sendMessage("§eYour gamemode has been updated to §lSpectator§e.");
                        break;
                    }
                    default: {
                        player.sendMessage("§cUnknown gamemode.");
                        break;
                    }
                }
            }
        } else {
            sender.sendMessage("§cYou must be a player to execute this command.");
        }
        return true;
    }
}
