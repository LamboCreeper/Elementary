package uk.lambocreeper.elementary.commands.home;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import uk.lambocreeper.elementary.Main;

public class TeleportHome implements CommandExecutor {
    private Plugin plugin = Main.getPlugin(Main.class);
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 0) {
                if (plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[0]) != null) {
                    String w = plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[0] + ".w");
                    Double x = plugin.getConfig().getDouble("home." + player.getUniqueId().toString() + "." + args[0] + ".x");
                    Double y = plugin.getConfig().getDouble("home." + player.getUniqueId().toString() + "." + args[0] + ".y");
                    Double z = plugin.getConfig().getDouble("home." + player.getUniqueId().toString() + "." + args[0] + ".z");
                    player.teleport(new Location(plugin.getServer().getWorld(w), x, y, z));
                    player.sendMessage("§eTeleported to " + args[0]);
                } else {
                    player.sendMessage("§cA home named " + args[0] + " does not exist.");
                }
            } else {
                player.sendMessage("§cYou must define a home to teleport to.");
            }
        } else {
            sender.sendMessage("§cYou must be a player to execute this command.");
        }
        return true;
    }
}
