package uk.lambocreeper.elementary.commands.home;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import uk.lambocreeper.elementary.Main;

public class RemoveHome implements CommandExecutor {
    private Plugin plugin = Main.getPlugin(Main.class);
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 0) {
                if (plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[0]) != null) {
                    plugin.getConfig().set("home." + player.getUniqueId().toString() + "." + args[0], null);
                    plugin.saveConfig();
                    player.sendMessage("§eYour old home, " + args[0] + ", has been removed.");
                } else {
                    player.sendMessage("§cA home named " + args[0] + " does not exist.");
                }
            } else {
                player.sendMessage("§cYou must define a name for your home.");
            }
        } else {
            sender.sendMessage("§cYou must be a player to execute this command.");
        }
        return true;
    }
}
