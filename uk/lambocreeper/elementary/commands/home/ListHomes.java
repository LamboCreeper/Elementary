package uk.lambocreeper.elementary.commands.home;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import uk.lambocreeper.elementary.Main;

public class ListHomes implements CommandExecutor {
    private Plugin plugin = Main.getPlugin(Main.class);
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (plugin.getConfig().contains("home." + player.getUniqueId().toString())) {
                player.sendMessage("§eYou have the following homes:");
                for (String home : plugin.getConfig().getConfigurationSection("home." + player.getUniqueId().toString()).getKeys(false)) {
                    player.sendMessage("§e● " + home);
                }
            } else {
                player.sendMessage("§cYou do not have any homes.");
            }
        } else {
            sender.sendMessage("§cYou must be a player to execute this command.");
        }
        return true;
    }
}
