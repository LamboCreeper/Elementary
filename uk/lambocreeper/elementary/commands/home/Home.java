package uk.lambocreeper.elementary.commands.home;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import uk.lambocreeper.elementary.Main;

import java.util.ArrayList;

public class Home implements CommandExecutor {
    private Plugin plugin = Main.getPlugin(Main.class);
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                TextComponent _title = new TextComponent("§6Elementary §7(Homes)\n");
                _title.setColor(ChatColor.GRAY);
                _title.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://lambocreeper.uk/elementary/docs?ref=mc&cmd=home"));
                _title.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7§oClick to view the documentation for this command.").create()));

                TextComponent _about = new TextComponent("§eSave and teleport to home.");
                _about.setColor(ChatColor.YELLOW);

                player.spigot().sendMessage(new TextComponent[]{_title, _about});
            } else {
                switch(args[0].toUpperCase()) {
                    case "S":
                    case "N":
                    case "A":
                    case "ADD":
                    case "NEW":
                    case "SET": {
                        if (args.length > 1) {
                            if (plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[1]) == null) {
                                plugin.getConfig().set("home." + player.getUniqueId().toString() + "." + args[1] + ".x", player.getLocation().getX());
                                plugin.getConfig().set("home." + player.getUniqueId().toString() + "." + args[1] + ".y", player.getLocation().getY());
                                plugin.getConfig().set("home." + player.getUniqueId().toString() + "." + args[1] + ".z", player.getLocation().getZ());
                                plugin.getConfig().set("home." + player.getUniqueId().toString() + "." + args[1] + ".w", player.getLocation().getWorld().getName());
                                plugin.saveConfig();
                                player.sendMessage("§eYour new home, " + args[1] + ", has been set to your current location.");
                            } else {
                                player.sendMessage("§cA home named " + args[1] + " already exists.");
                            }
                        } else {
                            player.sendMessage("§cYou must define a name for your home.");
                        }
                        break;
                    }
                    case "M":
                    case "D":
                    case "R":
                    case "MINUS":
                    case "DELETE":
                    case "REMOVE": {
                        if (args.length > 1) {
                            if (plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[1]) != null) {
                                plugin.getConfig().set("home." + player.getUniqueId().toString() + "." + args[1], null);
                                plugin.saveConfig();
                                player.sendMessage("§eYour old home, " + args[1] + ", has been removed.");
                            } else {
                                player.sendMessage("§cA home named " + args[1] + " does not exist.");
                            }
                        } else {
                            player.sendMessage("§cYou must define a name for your home.");
                        }
                        break;
                    }
                    case "T":
                    case "TP":
                    case "TELEPORT": {
                        if (args.length > 1) {
                            if (plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[1]) != null) {
                                String w = plugin.getConfig().getString("home." + player.getUniqueId().toString() + "." + args[1] + ".w");
                                Double x = plugin.getConfig().getDouble("home." + player.getUniqueId().toString() + "." + args[1] + ".x");
                                Double y = plugin.getConfig().getDouble("home." + player.getUniqueId().toString() + "." + args[1] + ".y");
                                Double z = plugin.getConfig().getDouble("home." + player.getUniqueId().toString() + "." + args[1] + ".z");
                                player.teleport(new Location(plugin.getServer().getWorld(w), x, y, z));
                                player.sendMessage("§eTeleported to " + args[1]);
                            } else {
                                player.sendMessage("§cA home named " + args[1] + " does not exist.");
                            }
                        } else {
                            player.sendMessage("§cYou must define a home to teleport to.");
                        }
                        break;
                    }
                    case "L":
                    case "LIST": {
                        if (plugin.getConfig().contains("home." + player.getUniqueId().toString())) {
                            player.sendMessage("§eYou have the following homes:");
                            for (String home : plugin.getConfig().getConfigurationSection("home." + player.getUniqueId().toString()).getKeys(false)) {
                                player.sendMessage("§e● " + home);
                            }
                        } else {
                            player.sendMessage("§cYou do not have any homes.");
                        }
                    }
                    default: {
                        player.sendMessage("§cUnknown argument.");
                        break;
                    }
                }
            }
        } else {
            sender.sendMessage("§cYou must be a player to execute this command.");
        }
        return true;
    }
}
