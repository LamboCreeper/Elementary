package uk.lambocreeper.elementary.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Elementary implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                TextComponent _title = new TextComponent("§6Elementary §7(Help)\n");
                _title.setColor(ChatColor.GRAY);
                _title.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,"http://lambocreeper.uk/elementary?ref=mc"));
                _title.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7§oClick to visit the Elementary website.").create()));

                TextComponent _about = new TextComponent("§eElementary adds fundamental features to a Spigot server.");
                _about.setColor(ChatColor.YELLOW);
                _about.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/elementary about"));
                _about.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7§oClick to learn more about Elementary.").create()));

                player.spigot().sendMessage(new TextComponent[]{_title, _about});
            } else {
                switch(args[0].toUpperCase()) {
                    case "ABOUT": {
                        player.sendMessage("Something...");
                    }
                    default: {
                        break;
                    }
                }
            }
        } else {
            sender.sendMessage("§cYou must be a player to execute this command.");
        }
        return true;
    }
}
