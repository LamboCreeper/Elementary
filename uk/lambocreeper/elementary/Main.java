package uk.lambocreeper.elementary;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import uk.lambocreeper.elementary.commands.*;
import uk.lambocreeper.elementary.commands.fun.Shrug;
import uk.lambocreeper.elementary.commands.fun.Tableflip;
import uk.lambocreeper.elementary.commands.fun.Unflip;
import uk.lambocreeper.elementary.commands.gamemode.*;
import uk.lambocreeper.elementary.commands.home.*;


public class Main extends JavaPlugin {
    public void onEnable() {
        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        console.sendMessage("§aYou are running §6Elementary v0.1.2§a by LamboCreeper.");

        saveDefaultConfig();
        getConfig().createSection("homes");

        for (String uuid : getConfig().getConfigurationSection("homes").getKeys(false)) {
            for (String home : getConfig().getConfigurationSection("homes." + uuid).getKeys(false)) {
                double x = getConfig().getDouble("homes." + uuid + "." + home + ".x");
                double y = getConfig().getDouble("homes." + uuid + "." + home + ".y");
                double z = getConfig().getDouble("homes." + uuid + "." + home + ".z");
                double w = getConfig().getDouble("homes." + uuid + "." + home + ".w");
            }
        }

        getServer().getPluginCommand("elementary").setExecutor(new Elementary());
        getServer().getPluginCommand("e").setExecutor(new Elementary());

        // <editor-fold desc="Gamemode Commands">
        getServer().getPluginCommand("gamemode").setExecutor(new Gamemode());
        getServer().getPluginCommand("gm").setExecutor(new Gamemode());
        getServer().getPluginCommand("survival").setExecutor(new Survival());
        getServer().getPluginCommand("gm0").setExecutor(new Survival());
        getServer().getPluginCommand("creative").setExecutor(new Creative());
        getServer().getPluginCommand("gm1").setExecutor(new Creative());
        getServer().getPluginCommand("adventure").setExecutor(new Adventure());
        getServer().getPluginCommand("gm2").setExecutor(new Adventure());
        getServer().getPluginCommand("spectator").setExecutor(new Spectator());
        getServer().getPluginCommand("gm3").setExecutor(new Spectator());
        // </editor-fold>

        getServer().getPluginCommand("fly").setExecutor(new Fly());

        // <editor-fold desc="Message Commands">
        getServer().getPluginCommand("message").setExecutor(new Message());
        getServer().getPluginCommand("msg").setExecutor(new Message());
        getServer().getPluginCommand("tell").setExecutor(new Message());
        getServer().getPluginCommand("w").setExecutor(new Message());
        getServer().getPluginCommand("whisper").setExecutor(new Message());
        // </editor-fold>

        getServer().getPluginCommand("me").setExecutor(new Me());

        // <editor-fold desc="Fun Commands">
        getServer().getPluginCommand("shrug").setExecutor(new Shrug());
        getServer().getPluginCommand("tableflip").setExecutor(new Tableflip());
        getServer().getPluginCommand("unflip").setExecutor(new Unflip());
        // </editor-fold>

        // <editor-fold desc="Home Commands">
        getServer().getPluginCommand("home").setExecutor(new Home());
        getServer().getPluginCommand("sethome").setExecutor(new SetHome());
        getServer().getPluginCommand("removehome").setExecutor(new RemoveHome());
        getServer().getPluginCommand("teleporthome").setExecutor(new TeleportHome());
        getServer().getPluginCommand("tphome").setExecutor(new TeleportHome());
        getServer().getPluginCommand("listhomes").setExecutor(new ListHomes());
        // </editor-fold>
    }
    public void onDisable() {
        saveConfig();
    }
}
